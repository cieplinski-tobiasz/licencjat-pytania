# Sposoby przekazywania parametrów do funkcji (przez wartość, przez referencję). Zalety i wady

## Przekazywanie przez wartość

Gdy parametr do funkcji jest przekazywany przez wartość, zarówno funkcja wywołująca jak i wywoływana posiadają własne kopie obiektów. Kopie te są tworzone tylko na czas istnienia funkcji i przestają istnieć po jej zakończeniu.

Konsekwencją tego jest to, że wszelkie zmiany parametru wewnątrz funkcji wywoływanej nie spowodują zmian w obiekcie po stronie funkcji wywołującej.

### Zalety

* przypadkowe zmiany obiektu wewnątrz funkcji nie wpływają na stan obiektu oryginalnego
* ograniczanie ilości efektów ubocznych

### Wady

* gdy przekazywany obiekt zajmuje dużo pamięci, koszt kopiowania takiego obiektu jest wysoki
* brak możliwości zmiany przekazanego obiektu

### Uwaga co do Javy

W języku Java wszystkie wartości są przekazywane przez wartość, jednak deklaracja `Object x = new Object();` powoduje, że w zmiennej `x` przechowywana jest **referencja** do obiektu x. Dlatego wywołanie metody, która modyfikuje stan obiektu przekazanego jako parametr spowoduje zmianę tego stanu również w obiekcie w funkcji wywołującej, ponieważ jest to ten sam obiekt.

## Przekazywanie przez referencję

W przypadku, gdy parametr do funkcji przekazywany jest przez referencję, funkcja wywołująca i wywoływana używają tej samej zmiennej. Wszelkie zmiany parametru wewnątrz funkcji są widoczne w obiekcie w funkcji wywołującej.

### Zalety

* możliwość zmiany przekazanego obiektu
* oszczędność pamięci i czasu potrzebnego na kopiowanie obiektów

### Wady

* potencjalne efekty uboczne w oryginalnym obiekcie w wyniku zmian wewnątrz funkcji