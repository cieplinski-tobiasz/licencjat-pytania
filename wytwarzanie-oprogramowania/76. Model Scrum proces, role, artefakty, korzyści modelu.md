# Model Scrum: proces, role, artefakty, korzyści modelu

## Scrum

Adaptacyjna metodyka wytwarzania oprogramowania zgodna z manifestem Agile.

## Korzyści

* szybkie reagowanie na zmieniające się wymagania klientów
* wymuszona komunikacja między członkami zespołu (daily scrum)
* szybka ocena tego w jakim momencie wytwarzania danego produktu jest zespół
* praca i organizacja zespołu pozostawiona developerom

## Artefakty

### Rejestr produktu (backlog)

Uporządkowana lista wszystkiego, co może być potrzebne w produkcie oraz jedyne źródło wymaganych zmian, które mają zostać wprowadzone. Odpowiedzialny za niego jest Właściciel Produktu. 

Jego elementy posiadają nastęþujące atrybuty: opis, kolejność i oszacowanie.

### Rejestr Sprintu

Podzbiór elementów backlogu wybranych do Sprintu rozszerzony o plan dostarczenia przyrostu produktu. Definiuje pracę, jaką zespół wykona by przekształcić elementy backlogu w ukończony przyrost.

Rejestr tworzy dobrze widoczny obraz pracy, jaką zespół deweloperski planuje wykonać w trakcie Sprintu. Należy on tylko i wyłącznie do zespołu.

### Daily Scrum

Służy do monitorowania postępów Sprintu

### Przyrost

Suma wszystkich elementów backlogu zakończonych podczas Sprintu i wszystkich poprzednich Sprintów. Na koniec Sprintu nowy przyrost musi być ukończony.

## Role

### Właściciel produktu (Product Owner)

Odpowiada za maksymalizację wartości produktu i pracy zespołu deweloperskiego. Sposób realizacji tej strategii może być różny pomiędzy poszczególnymi product ownerami.

Jest jedyną osobą zarządzającą Rejestrem Produktu (backlog), co rozumiemy przez:

* jasne artykułowanie elementów backlogu i określanie ich kolejności
* zapewnianie, że backlog jest dostępny, przejrzysty oraz jasny dla wszystkich

### Zespół deweloperski (Development Team)

Jest samoorganizującym się, zazwyczaj 3-9 osobowym zespołem, złożonym z profesjonalistów. Ma za zadanie dostarczenie gotowego do wydania przyrostu produktu. Zwykle jest wielofunkcyjny, gdyż pożądane jest, aby każda osoba w zespole posiadała wiedzę nie tylko ze swojej dziedziny.

Scrum nie przewiduje tytułów innych niż *deweloper* dla członków zespołu. Odpowiedzialność za wykonywaną pracę ponosi cały zespół - nie składa się on z podzespołów.

### Scrum Master

Odpowiedzialny za odpowiednie rozumienie i stosowanie Scruma przez upewnianie się, że zespół stosuje się do założeń teorii Scruma, jego praktyk i reguł postępowania.

## Proces

W ramach metodyki Scrum rozwój produktu dzielony jest na *sprinty*, czyli 2-4 tygodniowe okresy pracy nad postawionymi zadaniami.

Cały proces rozpoczyna się od planowania, które jest ograniczone czasowo - 8h dla miesięcznego sprintu - podczas którego ustala się co będzie zrobione w danym sprincie na bazie backlogu produktu, ostatniego przyrostu, pojemności zespołu developerskiego oraz ostatnich odczytów wydajności.

W czasie sprintu wytwarzany jest przyrost ukończonej, używalnej i potencjalnie gotowej do wydania funkcjonalności. Sprint ma stałą długość przez cały okres trwania prac i niedozwolone są zmiany wpływające na cel sprintu. Jedynie właściciel produktu ma prawo przerwać sprint, jeśli jego cel się zdezaktualizuje lub kontynuowanie prac nie ma sensu w zaistniałych okolicznościach. Przerwanie sprintu zużywa jednak zasoby, ponieważ zespół musi się przegrupować i kolejny sprint musi zostać zaplanowany.

W czasie sprintu zespół developerski codziennie spotyka się na *Daily Scrum*, aby sprawdzić stan pracy, zdecydować co robić i powiadomić innych o ewentualnych problemach.

Na koniec sprintu przeprowadza się *Sprint Review*, która sprawdza jak wygląda przyrost produktu. Zbierany też jest feedback, aby interesariusze mieli jasny obraz tego, co zostało wykonane. Następnie w ramach podsumowania przeprowadzana jest *Sprint Retrospective*, gdzie zespół ma okazje przyjrzeć się procesowi, narzędziom, interakcjom i wyciągnąć wnioski w celu usprawnienia pracy.

![scrum lifecycle](assets/scrum.png)