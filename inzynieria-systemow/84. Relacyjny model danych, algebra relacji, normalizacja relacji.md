# Relacyjny model danych, algebra relacji, normalizacja relacji.

## Relacyjny model danych (wg Date’a)

Punktem wyjścia jest najmniejsza semantycznie, niepodzielna jednostka danych, zwana *skalarem*. Zbiór wartości skalarnych nazywamy *dziedziną*.

*Zmienna relacji* jest to nazwany obiekt, którego wartość zmienia się w czasie, zaś wartość takiej zmiennej w danej chwili nazywa się *wartością relacji* (termin relacja jest na ogół używany w znaczeniu pewnej konkretnej wartości relacji).

Relacja $R$ (wartość relacji) na zbiorze niekoniecznie różnych dziedzin $D_1, \dots, D_n$ składa się z dwóch części:

* nagłówek
  * zbiór par (nazwa-atrybutu, nazwa-dziedziny) takich, że każdy atrybut odpowiada dokładnie jednej wyjściowej dziedzinie (czyli tylko jedna dziedzina dla każdego atrybutu)
    * nazwy atrybutów muszą być różne
* treść
  * składa się z krotek
  * krotka jest zbiorem par (nazwa-atrybutu, wartość-atrybutu)
    * dla każdej nazwy atrybutu krotka zawiera dokładnie jedną parę (nazwa-atrybutu, wartość-atrybutu)

Tak zdefiniowana relacja jest też określana terminem relacja bazodanowa. Liczbę atrybutów w relacji nazywamy *stopniem* relacji $R$, zaś liczbę krotek określamy jako *liczebność* relacji $R$. Relację stopnia $n$ nazywamy relacją $n$-arną.

W praktyce termin nagłówek relacji często jest używany w znaczeniu zbioru nazw atrybutów. 

![img](assets/Relational_model_concepts_PL.png) 

### Własności relacji bazodanowej

* krotki nie powtarzają się
* krotki nie są uporządkowane
* atrybuty nie są uporządkowane
* wartości atrybutów są skalarne, czyli atomowe w ramach modelu
  * relację, która spełnia taki warunek nazywa się *relacją znormalizowaną (relacją w pierwszej postaci normalnej)*

### Rodzaje relacji

* nazwana
  * zmienna relacji zdefiniowana w systemie zarządzania bazą danych 
* podstawowa
  * relacja nazwana, która jest autonomiczna
  * nie jest pochodna
  * posiada niezależny byt w bazie danych
* wyprowadzana
  * relacja, którą można otrzymać ze zbioru nazwanych relacja za pomocą wyrażenia relacyjnego
* perspektywa
  * nazwana relacja pochodna
  * wirtualna, czyli reprezentowana wyłącznie przez definicję za pomocą innych nazwanych relacji
* snapshot
  * perspektywa rzeczywista (nie wirtualna)
* wynik zapytania
  * nienazwana relacja pochodna
  * powstaje w wyniku realizacji zapytania
  * nie ma trwałego bytu w bazie
* wynik pośredni
  * nienazwana relacja pochodna
  * powstaje w wyniku realizacji wyrażenie zagnieżdżonego w innym wyrażeniu

## Algebra relacji

Model teoretyczny służący do opisywania semantyki relacyjnych baz danych. Dziedzinę tej algebry stanowią relacje, zaś ich operatory zostały dobrane tak, aby odpowiadały typowym operacjom, które występują w zapytaniach podczas wyszukiwania informacji z tabel w bazie danych.Relacje są reprezentowane ich nazwami, a z każdą z relacji jest związany jej schemat, czyli ciąg różnych nazw atrybutów, np. $R(A, B, C) $.

### Podstawowe operacje

#### Typowe operacje teoriomnogościowe

Relacje muszą mieć ten sam schemat, zaś operacje wykonywane są na treściach relacji:

* suma zbiorów
* iloczyn zbiorów
* różnica zbiorów

#### Iloczyn kartezjański $R \times S$

Łączy każdy wiersz tabeli $R$ z każdym wierszem tabeli $S$.

Ponieważ argumenty mogą mieć atrybuty o tych samych nazwach, nazwy atrybutów w schemacie wyniku trzeba czasem poprzedzać nazwami relacji z których pochodzą, np. $R(A, B, C)$ i $S(C, D, E)$ da nam $R \times S (A, B, R.C, S.C, D, E)$.

#### Selekcja (wybór) $\sigma_{warunek}(R)$

Polega na wybraniu z relacji tylko tych krotek, dla których spełniony jest podany warunek.

#### Rzutowanie (projekcja) $\pi_{atrybut_1, \dots, atrybut_n}$

Wybiera z relacji tylko wskazane atrybuty.

#### Złączenie $R \Join_\theta S$

Operacja ta jest podobna do iloczynu kartezjańskiego, lecz łączy ze sobą tylko pary krotek spełniające podany warunek. Jest równoważna operacji $\sigma_\theta(R \times S)$.

#### Równozłączenie

Operacja złączenia z operatorem $\theta$ równym operatorowi $=$.

#### Złączenie naturalne $R \Join S$

Operacja złączenia z warunkiem równym operatorowi równości *na wszystkich wspólnych atrybutach*.

#### Złączenia zewnętrzne

##### Lewe $R \Join_{LO} S$

Operacja zwraca wszystkie krotki z relacji $R$ i odpowiadające wartości z relacji $S$ po złączeniu. W przypadku braku dopasowanej krotki z relacji $S$, w atrybutach należących do $S$ wpisujemy `null`.

##### Prawe $R \Join_{RO} S$

Wynikiem tej operacji są wszystkie krotki z relacji $S$ i odpowiadające wartości z relacji $R$ po złączeniu. W przypadku braku dopasowanej krotki z relacji $R$, w atrybutach należących do $R$ wpisujemy `null`.

##### Pełne $R \Join_{FO} S$

Operacja zwraca wszystkie krotki z obu relacji. 

W przypadku braku dopasowanej krotki z relacji $S$, w atrybutach należących do $S$ wpisujemy `null`, zaś jeśli brakuje dopasowanej krotki z relacji $R$, w atrybutach należących do $R$ wpisujemy `null`.

Można traktować tę operację jako sumę lewego i prawego złączenia zewnętrznego: $R \Join_{FO} S = (R \Join_{LO} S) \cup (R \Join_{RO} S)$.

#### Półzłączenie $R \ltimes S$

Zwracany jest wynik operacji złączenia, z tym, że zwraca krotki należące tylko do jednej relacji. Jest równoważna operacji $\pi_{R_1, \dots, R_n}(R \Join S)$, gdzie $R_1, \dots, R_n$ to atrybuty relacji $R$.

#### Antyzłączenie $R \rhd S$

W wyniku działania otrzymujemy krotki relacji $R$, które nie "zakwalifikowały się" do złączenia, czyli nie spełniły podanego warunku.

Antyzłączenie definiowane jest jako $R \rhd S = R - R \ltimes S$.

#### Dzielenie $R \div S$

Rezultatem tego działania są atrybuty, które należą do $R$ i nie należą do $S$ i krotki, które należą do $R$ i nie należą do $S$.

## Normalizacja relacji

Proces organizacji danych w bazie danych powodujący redukcję redundancji, która jest niekorzystna, ponieważ zużywa miejsce na dysku, ale co ważniejsze jej obecność może prowadzić do różnych anomalii:

* anomalia aktualizacji - dane mogą być niespójne, ponieważ nie są aktualizowane we wszystkich miejscach
* anomalia wstawiania - niemożność dodania pewnych danych bez dodawania innych
* anomalia usuwania - niemożność usunięcia pewnych danych bez usuwania innych

### **Postać normalna**

Jest to postać relacji w bazie danych, w której nie występuje redundancja (nadmiarowość), czyli powtarzanie się tych samych informacji. Doprowadzenia relacji do postaci normalnej nazywa się normalizacją (lub dekompozycją) bazy danych. 