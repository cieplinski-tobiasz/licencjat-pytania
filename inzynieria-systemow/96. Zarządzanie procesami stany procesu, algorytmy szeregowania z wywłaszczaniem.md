# Zarządzanie procesami: stany procesu, algorytmy szeregowania z wywłaszczaniem.

## Stany procesu

* nowy
  * formowanie procesu, czyli gromadzenie zasobów niezbędnych do rozpoczęcia wykonywania procesu z wyjątkiem kwantu czasu procesora
  * po zakończeniu formowania oczekiwanie na przyjęcie do kolejki procesów gotowych
* uśpiony - oczekujący na wykonanie
* aktualnie wykonywany - wykonywanie instrukcji programu danego procesu i wynikająca z ich wykonywania zmiana stanu odpowiednich zasobów systemu
* oczekujący - zatrzymanie wykonywania instrukcji programu danego procesu ze względu na potrzebę przydziału dodatkowych zasobów, konieczność otrzymania danych lub osiągnięcia odpowiedniego stanu przez otoczenie procesu
* zatrzymany - `ctrl+z`
* gotowy - oczekiwanie na przydział kwantu czasu procesora
* zakończony
  * zakończenie wykonywania programu
  * zwolnienie większości zasobów
  * oczekiwanie na możliwość przekazania informacji o zakończeniu do jądra systemu operacyjnego lub innym procesom
* sierota - proces potomny, którego przodek już się zakończył
* zombie - proces, którego wykonanie zostało zakończone przez system operacyjny, jednak jego zamknięcie nie zostało jeszcze obsłużone przez rodzica

![unix-processes](assets/unix-processes.png) 

## Dyspozytor (scheduler)

Część systemu operacyjnego odpowiedzialna za przydzielanie czasu procesora w ramach przełączania zdań.

## Algorytmy szeregowania

### First In First Out (FIFO)

Zadanie wykonuje się aż nie zostanie wywłaszczone przez siebie lub inne zadanie o wyższym priorytecie.

Algorytm powszechnie stosowany, jeden z prostszych w realizacji, dający dobre efekty w systemach ogólnego przeznaczenia.

### Planowanie rotacyjne (round-robin, algorytm karuzelowy)

Każde z zadań otrzymuje kwant czasu, następnie po spożytkowaniu swojego kwantu zostaje wywłaszczone i ustawione na końcu kolejki.

### Planowanie sporadyczne

Zadania otrzymują tzw. "budżet czasu". Algorytm ten pomaga pogodzić wykluczające się reguły dotyczące szeregowania zdań okresowych i nieokresowych.
