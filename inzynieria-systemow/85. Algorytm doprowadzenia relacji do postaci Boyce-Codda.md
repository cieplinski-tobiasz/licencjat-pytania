# Algorytm doprowadzenia relacji do postaci Boyce-Codda.

Relacja jest w postaci normalnej Boyce-Codda wtedy i tylko wtedy, gdy dla każdej całkowicie nietrywialnej zależności funkcyjnej $X \to Y$, $X$ jest nadkluczem.

## Całkowita nietrywialność

Zależność funkcyjna $X \to Y$ jest całkowicie nietrywialna, wtedy i tylko wtedy, gdy $X \cap Y = \empty$.

## Nadklucz

Zbiór $X$ zawarty w zbiorze atrybutów relacji $R(A_1, \dots, A_n)$ jest nadkluczem wtedy i tylko wtedy, gdy zachodzi zależność funkcyjna $X \to A_1\dots A_n$.

## Algorytm

* znajdź wszystkie całkowicie nietrywialne zależności funkcyjne $X \to Y$ w relacji $R$ naruszające warunek Boyce-Codda, czyli takie, dla których $X$ nie jest nadkluczem
* zdekomponuj relację na dwie relacje: 
  * $R_1$, która zawiera atrybuty należące do zależności funkcyjnej naruszającej warunek Boyce-Codda
  * $R_2$, która zawiera wszystkie atrybuty z relacji $R$ z wyłączeniem atrybutów $Y$ po prawej stronie zależności funkcyjnej