# Lista: ujęcie abstrakcyjne, możliwe implementacje i ich złożoności

W ujęciu abstrakcyjnym lista jest skończonym ciągiem elementów ustalonego typu zwanego typem bazowym. Ważną własnością listy jest liniowe uporządkowanie jej elementów, zgodnie z ich pozycjami na liście, tj. mówimy, że element $a_i$ znajduje się na pozycji $i$ oraz, że element $a_i$ poprzedza $a_{i+1}$ dla $i = 1, 2, \dots, n-1$, gdzie $n$ to długość listy.

W przypadku, gdy ten sam element występuje w liście wiele razy, każde wystąpienie traktowane jest jak unikalna wartość.

Interfejs listy umożliwia podstawowe operacje dostępu do danych:

* wstawianie
* usuwanie
* wyszukiwanie

## Implementacje

### Tablicowa

Polega na używaniu tablicy jako kontenera przechowującego elementy listy. Wadą tej implementacji jest konieczność powiększania tablicy, gdy skończy się wolne miejsce na dodatkowe elementy, a także przymus kopiowania fragmentów tablicy w przypadku gdy element jest wstawiany lub usuwany ze środka listy.

#### Złożoności

| Operacja     | Złożoność czasowa |
| ------------ | ----------------- |
| dostęp       | $O(1)$            |
| wyszukiwanie | $O(n)$            |
| wstawianie   | $O(n)$            |
| usuwanie     | $O(n)$            |

### Wiązana

Implementacja używająca struktury pojedynczo lub podwójnie wiązanej do połączenia obiektów typu `Link`. Dostęp bezpośredni jest możliwy zazwyczaj tylko do elementu pierwszego. Warunek `first == null` oznacza, że lista jest pusta.

W liście pojedynczo wiązanej każda instancja obiektu `Link` zawiera informację odnośnie wartości danego elementu, oraz referencję do następnego elementu w liście. W ostatnim obiekcie `Link`, referencja do kolejnego elementu wskazuje na `null`.

W liście podwójnie wiązanej każdy obiekt `Link` zawiera również referencję na element poprzedni, co umożliwia obustronne poruszanie się po liście.

Istnieją również modyfikacja dwóch powyższych standardowych implementacji listy na strukturze wiązanej:

* lista dwustronna
  * przechowuje dodatkową referencję `last` do ostatniego elementu
  * pozwala bezpośrednio wstawić element na koniec listy bez konieczności przechodzenia jej całej
* lista z głową (nagłówkiem)
  * `head` to dodatkowa komórka na początku listy bez własnej wartości, która wskazuje na faktyczny pierwszy element listy
  * upraszcza wstawianie i usuwanie, ponieważ zawsze istnieje element poprzedni
* lista cykliczna
  * ostatnia komórka wskazuje pierwszy element jako następny element
  * przydatna w sytuacjach, gdy lista przeglądana jest w całości, ale niekoniecznie od początku

#### Złożoności
| Operacja     | Złożoność czasowa |
| ------------ | ----------------- |
| dostęp       | $O(n)$            |
| wyszukiwanie | $O(n)$            |
| wstawianie   | $O(1)$            |
| usuwanie     | $O(1)$            |

