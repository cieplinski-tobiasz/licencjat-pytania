# Sposoby reprezentowania liczb w komputerze

## Stałopozycyjne:

#### ZM (Znak - Moduł):

Pierwszy od lewej jest bit znaku 0 - liczba dodatnia, 1 - ujemna. Wobec tego istnieją dwie reprezentacje zera, co jest problemem.

Przykład: Liczba 

- 26 =  0 0 0 1 1 0 1 0 na 8 bitach,
- -26 = 1 0 0 1 1 0 1 0 na 8 bitach.

#### Kod U1 (uzupełnienie do 1):

Liczby dodatnie bez zmian, ujemne zapisujemy w ten sposób, chcemy zapisać -27:

1. 0 0 0 1 1 0 1 1 - Zapisz liczbę dodatnią 27.
2. 1 1 1 0 0 1 0 0 - Dokonaj inwersji bitów.

#### Kod U2 (uzupełnienie do 2):

Dodatnie bez zmian, algorytm ujemnych poniżej:

1. Zapisz liczbę przeciwną do danej tak jak w ZM. 
2. Dokonaj inwersji bitów (czyli pozamieniaj 0 na 1 i odwrotnie:);
3. Zwiększ wynik o 1.

Chcemy zapisać - 27: 

1. 0 0 0 1 1 0 1 1 ~ 1+ 2 + 8 + 16 - Zapisujemy liczbę 27 na 8 bitach.
2. 1 1 1 0 0 1 0 0 ~ Inwersja bitów
3. 1 1 1 0 0 1 0 1 ~ zwiększamy dopisujemy na końcu 1 i mamy -27 w $U_2$ 

![1530219828152](assets/1530219828152.png)

## Zmiennopozycyjne:

Liczby są reprezentowane w postaci $m \times q^c$ , gdzie m - mantysa, q - podstawa systemu liczbowego ()u nas 2), c - cecha

Załóżmy, że chcemy przedstawić liczbę 20.75 w postaci zmiennopozycyjnej:

Tak wygląda to w notacji ZM, oddzielony jest bit znaku: 0|10100.11

Są 3 opcje normalizacji:

1. Mantysa nie zawiera części ułamkowej 0|1010011
2. Mantysa zawiera tylko jeden znak części całkowitej (jedna jedynka). 0|1.010011
3. Mantysa jest w całości ułamkiem 0|.1010011

E oznacza system dziesiętny, D - dwójkowy

![1530227995704](assets/1530227995704.png)

Załóżmy, że używamy normalizacji nr 3.

Na początku mieliśmy 0|10100.11, a chcemy mieć przecinek przesunięty o 5 miejsc w lewo, czyli tak 0|.1010011, to znaczy, że cecha musi zawierać informację o ile miejsc należy przesunąć przecinek, by otrzymać pierwotną liczbę, w naszym przypadku jest to 5 miejsc, czyli w ZM 101, czyli w rejestrze będziemy mieli 0|.1010011 0|101

Całość jest oczywiście znacznie bardziej popierdolona, gdy używamy różnych systemów kodowania omówionych wyżej do cechy i mantysy, bo trzeba to mieć na względzie przy kodowania i dekodowaniu.